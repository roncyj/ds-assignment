/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import static main.ServerMain.getGroup;
import static main.ServerMain.getUser;

/**
 *
 * @author Angelyu
 */
public class Database {

    File file = new File("UserList.db");

    public Database() {
    }

    //Read Username/Group and password from database
    public void readDatabase() throws IOException {
        if (!file.exists()) {
            file.createNewFile();
            getGroup().add(new Group("Public"));
            writeDatabase();
        }
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String content;
        ServerMain.serverClass.user = new ArrayList();
        ServerMain.serverClass.group = new ArrayList();
        
        while ((content = reader.readLine()) != null) {
            String[] temp = content.split("[;]");
            if (temp[0].equalsIgnoreCase("User")) {
                getUser().add(new User(temp[1], temp[2]));
            } else if (temp[0].equalsIgnoreCase("Group")) {
                String[] list = new String[temp.length - 2];
                for (int i = 2; i < temp.length; i++) {
                    list[i - 2] = temp[i];
                }
                getGroup().add(new Group(temp[1], list));
            }
        }
        reader.close();
    }

    //Write all the user and group information into files
    public void writeDatabase() throws FileNotFoundException, IOException {
        if (!file.exists()) {
            file.createNewFile();
        }
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file, false)));
        for (int i = 0; i < getUser().size(); i++) {
            if (!getUser().get(i).getUserName().contains("Anonymous")) {
                writer.println("User;" + getUser().get(i).getUserName() + ";" + getUser().get(i).getUserPassword());
            }
        }
        for (int i = 0; i < getGroup().size(); i++) {
            writer.println("Group;" + getGroup().get(i).getGroupName() + ";" + getGroup().get(i).getMemberListStr());
        }
        writer.close();

    }

}
