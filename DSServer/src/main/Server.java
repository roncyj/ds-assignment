/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RonCYJ
 */
public class Server implements Runnable {

    ExecutorService execSocket = Executors.newCachedThreadPool();
    public ArrayList<User> user = new ArrayList();
    public ArrayList<Group> group = new ArrayList();
    public ServerSocket serverSocket;
    boolean interrupt = false;
    boolean foundPort = false;
    int numOfConnection = 0;

    @Override
    public void run() {
        try {
            // Add User and group
//            user.add(new User("User1", "user", new String[]{"user2", "user3"}, new String[]{"Hey", "Yo"}));
//            user.add(new User("User2", "user", new String[]{"user2", "user3"}, new String[]{"Hey", "Yo"}));
//            user.add(new User("User3", "user", new String[]{"user2", "user3"}, new String[]{"Hey", "Yo"}));
//            user.add(new User("User4", "user", new String[]{"user2", "user3"}, new String[]{"Hey", "Yo"}));
//            user.add(new User("User5", "user", new String[]{"user2", "user3"}, new String[]{"Hey", "Yo"}));
//
//            group.add(new Group("Groupy", new String[]{"User1", "User2", "User3", "User5"}));
//            database.writeDatabase();

            // Read all the user and group info from the database
            ServerMain.database.readDatabase();
            ServerMain.controller.updateText("Added User\n-------------------------------");

            //Look for available port to start server
            while (!foundPort) {
                try {
                    //Create a server socket to accept Messaging connection
                    serverSocket = new ServerSocket(ServerMain.port);
                    foundPort = true;
                    System.out.println("Server Running");
                    ServerMain.controller.updateText("Server Running\nPort: " + ServerMain.port + "\n-------------------------------");

                    //Start Offline Messages Handler thread
                    ServerMain.offMsgHandler.start();
                    ServerMain.controller.refreshStatusTable();

                    //Auto create new thread to handle each client request
                    while (!interrupt) {
                        numOfConnection++;
                        // A new thread will create to deal with each socket client
                        execSocket.execute(new ServerThread(serverSocket.accept(), numOfConnection));
                        ServerMain.controller.updateText("New User try Login");
                        if (ServerMain.port != 8080) {
                            ServerMain.database.readDatabase();
                        }
                    }
                } catch (IOException ex) {
                    System.out.println("Port already in use");
                    ServerMain.port++;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the user
     */
    public ArrayList<User> getUser() {
        return user;
    }

    // Find User index with the user name
    public int getUserNum(String name) {
        int i = 0;
        boolean flag = false;
        for (; i < user.size(); i++) {
            if (user.get(i).getUserName().equalsIgnoreCase(name)) {
                return i;
            }
        }
        return -1;
    }

    // Find Group index with the group name
    public int getGroupNum(String name) {
        int i = 0;
        boolean flag = false;
        for (; i < group.size(); i++) {
            if (group.get(i).getGroupName().equalsIgnoreCase(name)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * @param user the user to set
     */
    public synchronized void setUser(ArrayList user) {
        this.user = user;
    }

    /**
     * @return the group
     */
    public ArrayList<Group> getGroup() {
        return group;
    }

    /**
     * @param group the chatGroup to set
     */
    public void setGroup(ArrayList<Group> group) {
        this.group = group;
    }

    public void interrupt() {
        interrupt = true;
    }

    // Runnable Thread to Update the online status to all the client
    static class UpdateStatus implements Runnable {

        ArrayList<User> user = new ArrayList<>();
        ArrayList<Group> group = new ArrayList<>();
        PrintWriter out;

        public UpdateStatus(ArrayList<User> user, ArrayList<Group> group) {
            this.user = user;
            this.group = group;
        }

        @Override
        public void run() {
            synchronized (ServerMain.offMsgHandler) {
                ServerMain.offMsgHandler.notify();
            }
            String statusOutput = "";

            //Get all the Individual Status into a string
            for (int i = 0; i < user.size(); i++) {
                if (i != (user.size() - 1)) {
                    statusOutput = statusOutput + user.get(i).getUserName()
                            + ";Personal;" + user.get(i).getStatus() + ";";
                } else {
                    statusOutput = statusOutput + user.get(i).getUserName()
                            + ";Personal;" + user.get(i).getStatus();
                }
            }

            // Send the status of all the group and individual to all the client
            for (int i = 0; i < user.size(); i++) {
                String tempStatusOutput = statusOutput;
                // Get All the Group Status save into a string
                for (int j = 0; j < group.size(); j++) {
                    for (int k = 0; k < group.get(j).getMemberList().size(); k++) {
//                        System.out.println("Update Group name : "+group.get(j).getMemberList().get(k) + " Current User : " + user.get(i).getUserName()+ " Index: " +i+" "+j+" "+k );
                        if (group.get(j).getMemberList().get(k).equalsIgnoreCase(user.get(i).getUserName())) {
                            tempStatusOutput = group.get(j).getGroupName()
                                    + ";Group;Unknown;" + tempStatusOutput;
                            break;
                        }

                    }
                }
                try {
//                    System.out.println(user.size());
                    if (user.get(i).getStatus().equalsIgnoreCase("Online")) {
                        out = new PrintWriter(user.get(i).getSocket().getOutputStream(), true);
                        out.println(new Message("Status", user.get(i).getUserName(), "Server", tempStatusOutput));
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            synchronized (ServerMain.offlineMsgHandler) {
                ServerMain.offlineMsgHandler.notify();
            }
        }
    }

    // Runnable Thread to send an Individual message to the client
    static class IndividualMessage implements Runnable {

        User destination;
        PrintWriter out;
        Message inMsg;

        public IndividualMessage(User destination, Message inMsg) {
            this.destination = destination;
            this.inMsg = inMsg;
        }

        @Override
        public void run() {
            System.out.println("Sending Message");
            //Check whether the destination client is connected or not
            if (destination.getSocket().isConnected()) {
                System.out.println("" + inMsg.getDestination() + inMsg.getSource() + inMsg.toString());
                try {
                    out = new PrintWriter(destination.getSocket().getOutputStream(), true);
                    //Send Message to the destination client
                    out.println(inMsg.toString());
                } catch (IOException ex) {
                    Logger.getLogger(IndividualMessage.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("Is not connected");
                ServerMain.controller.updateText("Not Connected");
                ServerMain.offlineMsgHandler.addOfflineMsg(inMsg, destination.getUserName());
            }
        }
    }

    // A runnable Thread that send group message to all the person in the group list
    static class GroupMessage implements Runnable {

        Group destination;
        Message msg;
        PrintWriter out;
        int count = 0;

        public GroupMessage(Group destination, Message msg) {
            this.destination = destination;
            this.msg = msg;
        }

        @Override
        public void run() {
            System.out.println("Sending Message");
            System.out.println("" + ";" + msg.getSource() + ";" + msg.getContent());
            try {
                //Search for User who are in the list
                for (int i = 0; i < ServerMain.getUser().size(); i++) {
                    for (int j = 0; j < destination.getMemberList().size(); j++) {
                        //If the User match the group member list, it will send the message to the client
                        if (ServerMain.getUser().get(i).getUserName().equals(destination.getMemberList().get(j))) {
                            if (!ServerMain.getUser().get(i).getUserName().equalsIgnoreCase(msg.getSource())) {
                                if (ServerMain.getUser().get(i).getSocket().isConnected()) {
                                    out = new PrintWriter(ServerMain.getUser().get(i).getSocket().getOutputStream(), true);
                                    out.println(msg.toString());
                                } else {
                                    System.out.println("Is not connected");
                                    ServerMain.controller.updateText("Not Connected");
                                    ServerMain.offlineMsgHandler.addOfflineMsg(msg, ServerMain.getUser().get(i).getUserName());
                                }
                            }
                            count++;
                            break;
                        }
                    }
                    if (count == destination.getMemberList().size()) {
                        break;
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(GroupMessage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    static class SendFile implements Runnable {

        byte[] content;
        Socket destination;
        Message inMsg;

        public SendFile(Socket destination, byte[] content, Message inMsg) {
            this.content = content;
            this.destination = destination;
            this.inMsg = inMsg;
        }

        @Override
        public void run() {
            ObjectOutputStream oos = null;
            try {
                PrintWriter destOut = new PrintWriter(destination.getOutputStream(), true);
                System.out.println(inMsg.toString());
                
                //Send file to the destination User
                destOut.println(inMsg);
                
                //Wait for client to start input stream 
                synchronized (this) {
                    this.wait(500);
                }
                oos = new ObjectOutputStream(destination.getOutputStream());
                // byte[] content = Files.readAllBytes(file.toPath());
                oos.writeObject(content);
            } catch (IOException | InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    static class SendGroupFile implements Runnable {

        byte[] content;
        Group destination;
        Message msg;
        PrintWriter out;
        int count = 0;

        public SendGroupFile(Group destination, byte[] content, Message msg) {
            this.content = content;
            this.destination = destination;
            this.msg = new Message(msg.getType(), msg.getDestination(), msg.getSource(), msg.getContent());
        }

        @Override
        public void run() {
            System.out.println("Sending Group File");
            System.out.println("" + ";" + msg.getSource() + ";" + msg.getContent());
            
            //Search for any match with User name and group name
            for (int i = 0; i < ServerMain.getUser().size(); i++) {
                for (int j = 0; j < destination.getMemberList().size(); j++) {
                    //If the User match the group member list, it will send the message to the client
                    if ((ServerMain.getUser().get(i).getUserName().equals(destination.getMemberList().get(j)))) {
                        if (!ServerMain.getUser().get(i).getUserName().equalsIgnoreCase(msg.getSource())) {
                            ServerMain.execIO.execute(new SendFile(ServerMain.getUser().get(i).getSocket(), content, msg));
                        }
                        count++;
                        break;
                    }
                }
                if (count == destination.getMemberList().size()) {
                    break;
                }
            }
        }
    }

    static class OfflineMsgHandler implements Runnable {

        ArrayList<Message> offMsg = new ArrayList<>();
        ArrayList<String> destNameList = new ArrayList<>();
        File offMessages = new File("offMessages.txt");
        File offDestName = new File("offDestName.txt");
        PrintWriter pwOffMsg;
        PrintWriter pwOffName;
        BufferedReader brOffMsg;
        BufferedReader brOffName;
        private boolean interrupt = false;

        @Override
        public void run() {
            try {
                readFile();
                while (!isInterrupt()) {
                    System.out.println("Checking Offline Messages");
                    for (int i = 0; i < destNameList.size(); i++) {
                        User temp = ServerMain.getUser().get(ServerMain.getUserNum(destNameList.get(i)));
                        if (temp.getSocket().isConnected()) {
                            System.out.println("Sent Offline Message to " + temp.getUserName());
                            ServerMain.execIO.execute(new Server.IndividualMessage(temp, offMsg.get(i)));
                            offMsg.remove(i);
                            destNameList.remove(i);
                            i--;
                        }
                    }
                    writeFile();
                    synchronized (this) {
                        this.wait();
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Add offline message if can't reach the user
        public void addOfflineMsg(Message msg, String destName) {
            offMsg.add(msg);
            destNameList.add(destName);
            synchronized (this) {
                this.notify();
            }
        }

        // write the offline messages to the database for incase server down
        public void writeFile() throws IOException {
            this.pwOffMsg = new PrintWriter(new FileWriter(offMessages, false));
            this.pwOffName = new PrintWriter(new FileWriter(offDestName, false));
            if (!offMessages.exists()) {
                offMessages.createNewFile();
            }
            if (!offDestName.exists()) {
                offDestName.createNewFile();
            }
            for (int i = 0; i < offMsg.size(); i++) {
                pwOffMsg.println(offMsg.get(i).toString());
                pwOffName.println(destNameList.get(i));
            }
            pwOffMsg.close();
            pwOffName.close();
        }

        //Read offline messages
        public void readFile() throws FileNotFoundException, IOException {
            if (!offMessages.exists()) {
                offMessages.createNewFile();
            }
            if (!offDestName.exists()) {
                offDestName.createNewFile();
            }
            offMsg = new ArrayList<>();
            destNameList = new ArrayList<>();
            brOffMsg = new BufferedReader(new FileReader(offMessages));
            brOffName = new BufferedReader(new FileReader(offDestName));
            String readLine;
            while ((readLine = brOffMsg.readLine()) != null) {
                offMsg.add(new Message(readLine));
            }
            while ((readLine = brOffName.readLine()) != null) {
                destNameList.add(readLine);
            }
        }

        /**
         * @return the interrupt
         */
        public boolean isInterrupt() {
            return interrupt;
        }

        /**
         * @param interrupt the interrupt to set
         */
        public void setInterrupt(boolean interrupt) {
            this.interrupt = interrupt;
        }

        public void interrupt() {
            interrupt = !interrupt;
        }
    }
}
