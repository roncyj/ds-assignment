/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.File;
import static main.ServerMain.controller;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import static main.ServerMain.getUserNum;
import static main.ServerMain.getGroup;
import static main.ServerMain.getGroupNum;
import static main.ServerMain.getUser;

/**
 *
 * @author RonCYJ
 */
public class ServerThread implements Runnable {

    private Socket clientSocket = null;
    int connectionNum;
    String name;
    int userNum = 0;
    boolean flag = false;
    boolean anonLoginFlag = false;
    boolean interrupt = false;

    PrintWriter out;

    public ServerThread(Socket socket, int connectionNum) {
        clientSocket = socket;
        this.connectionNum = connectionNum;
    }

    @Override
    public void run() {
        try {
            //Intialize Printwriter and BufferedReader to the connected Client
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String inputLine;

            //Converted the received message into Object
            Message inMsg = new Message(in.readLine());
            System.out.println("Connected to Client on Connection " + connectionNum);
            System.out.println("Waiting for authentication");

            //Display the received message from client
//            controller.updateText(inMsg.toString());
            if (inMsg.getType().equalsIgnoreCase("SignUp")) {
                if (((userNum = getUserNum(inMsg.getSource())) == -1)&& (!inMsg.getSource().contains("Anonymous"))) {
                    System.out.println("New User Added");
                    controller.updateText("New User Added");
                    getUser().add(new User(inMsg.getSource(), inMsg.getContent()));
                    getGroup().get(0).addMemberList(inMsg.getSource());
                    ServerMain.database.writeDatabase();
                    out.println(new Message("SignUp", "", "", "Created"));
                    inMsg = new Message(in.readLine());
                }
            }

            if (inMsg.getType().equalsIgnoreCase("Login")) {
                // Anonymous Login 
                if (inMsg.getSource().equalsIgnoreCase("Anon")) {
                    controller.updateText("Anonymous Login");
                    anonLoginFlag = true;
                } else // Check whether the username in the user list
                if ((userNum = getUserNum(inMsg.getSource())) == -1) {
                    //Execute when Username is not found in the UserList
                    System.out.println("Can't find User name");
                    out.println(new Message("A0", "", "", "Denied").toString());
                    controller.updateText("Can't find User Name");
                    return;
                }

                // Create a new User for Anonumous login
                if (anonLoginFlag) {
                    User temp = new User("Anonymous" + (1 + (new Random().nextInt(1000))));
                    getUser().add(temp);
                    getGroup().get(0).addMemberList(temp.getUserName());
                    userNum = getUser().size() - 1;
                }

                // Get UserName from the Name list
                name = getUser().get(userNum).getUserName();
                if (getUser().get(userNum).getStatus().equalsIgnoreCase("Online")) {
                    System.out.println("Access Denied: LoggedIn");
                    out.println(new Message("Denied", "", "", "LoggedIn").toString());
                    return;
                }
                // Check the authentication of the client
                if ((inMsg.getSource().equalsIgnoreCase(name)
                        && inMsg.getContent().equalsIgnoreCase(getUser().get(userNum).getUserPassword())) || anonLoginFlag) {
                    //User is connected to server

                    // Logout the devices that connected to the system.
                    if (getUser().get(userNum).getStatus().equalsIgnoreCase("Online")) {
                        ServerMain.execIO.execute(new Server.IndividualMessage(getUser().get(userNum), new Message("Logout", "", "", "Multiple")));
                        synchronized (this) {
                            this.wait(500);
                        }
                    }
                    // Set Client socket info and status to the User List
                    getUser().get(userNum).setSocket(clientSocket);
                    getUser().get(userNum).setStatus("Online");

                    //Acknowledge the user login
                    //out.println(new Message("A0", getUser().get(userNum).getUserName(), "", "Success").toString());
                    controller.updateText("User is Connected to server");
                    System.out.println(name + " Connected");

                    //Refresh Status Table
                    controller.refreshStatusTable();
                    ServerMain.execIO.execute(new Server.UpdateStatus(getUser(), getGroup()));

                    // A loop to keep receive Client message
                    while (true) {
                        inMsg = new Message(in.readLine());
                        System.out.println(inMsg.toString());
                        switch (inMsg.getType()) {
                            case "IndChat":
                                //Process individual message
                                controller.updateText("Sending Individual message\nContent : " + inMsg.getContent());
                                ServerMain.execIO.execute(new Server.IndividualMessage(getUser().get(getUserNum(inMsg.getDestination())), inMsg));
                                break;
                            case "GroupChat":
                                //Process group message
                                controller.updateText("Sending Group Message");
                                ServerMain.execIO.execute(new Server.GroupMessage(getGroup().get(getGroupNum(inMsg.getDestination())), inMsg));
                                break;
                            case "SendFile":
                                sendFile(inMsg);
                                break;
                            case "SendGroupFile":
                                sendGroupFile(inMsg);
                                break;
                            case "NewGroup":
                                newGroup(inMsg);
                                break;
                            case "Logout":
                                //Sign Out
                                controller.updateText("Sign Out");
                                signOut();
                                break;
                            case "FileAcknowledge":
                                synchronized (this) {
                                    this.wait(500);
                                }
                                break;
                            default:
                                errorMessage("Input Error");
                        }
                        if (interrupt) {
                            break;
                        }
                    }
                } else {
                    System.out.println("Incorrect Password");
                    out.println(new Message("Error", "", "", "Denied").toString());
                    controller.updateText("Incorrect Password");
                }
            } else {
                System.out.println("Access Denied");
                out.println(new Message("Error", "", "", "Denied").toString());
            }
            System.out.println("Out");
            controller.updateText("User is disconnect from server");
            if ((userNum!= -1) &&(clientSocket.equals(getUser().get(userNum).getSocket()))) {
                getUser().get(userNum).setStatus("Offline");
            }

            // Update the offline status table and update all the client
            ServerMain.execIO.execute(new Server.UpdateStatus(getUser(), getGroup()));
            controller.refreshStatusTable();
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port " + "7" + " or listening for a connection");
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException | InterruptedException ex) {
            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendFile(Message inMsg) throws IOException, ClassNotFoundException {
        //File Transfer
        controller.updateText("File Transfer");
        //out.println(new Message("A0", "", "", "OK"));
        File f = new File(inMsg.getContent());
        //Create input stream and receive file from client
        ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
        byte[] content = (byte[]) ois.readObject();
        //Save file into server.
        Files.write(f.toPath(), content);
        //Pass the receive file to the destination client
        ServerMain.execIO.execute(new Server.SendFile(getUser().get(getUserNum(inMsg.getDestination())).getSocket(), content, inMsg));
//                                fileTransfer(inMsg[1]);
    }

    public void sendGroupFile(Message inMsg) throws IOException, ClassNotFoundException {
        //File Transfer
        controller.updateText("File Transfer");
        //out.println(new Message("A0", "", "", "OK"));
        File f2 = new File(inMsg.getContent());
        //Create input stream and receive file from client
        ObjectInputStream ois2 = new ObjectInputStream(clientSocket.getInputStream());
        byte[] content2 = (byte[]) ois2.readObject();
        //Save file into server.
        Files.write(f2.toPath(), content2);
        //Broadcast the file to other group member
        ServerMain.execIO.execute(new Server.SendGroupFile(getGroup().get(getGroupNum(inMsg.getDestination())), content2, inMsg));
//                                fileTransfer(inMsg[1]);
    }

    public void newGroup(Message inMsg) throws IOException {
        //New Group
        controller.updateText("New Group");
        if (getGroupNum(inMsg.getDestination()) == -1) {
            System.out.println("New User Added");
            controller.updateText("New User Added");
            getGroup().add(new Group(inMsg.getDestination(), inMsg.getContent().split("[;]")));
            ServerMain.database.writeDatabase();

            //Multiple message send to send receiving messages and acknoledgement
            //The first message will request client to allow next messages to be able to access by the groupu creation method
            out.println(new Message("NewGroup", "", "", "Created"));
            out.println(new Message("NewGroup", "", "", "Created"));

        } else {
            out.println(new Message("NewGroup", "", "", "Failed"));
            out.println(new Message("NewGroup", "", "", "Failed"));

        }
        ServerMain.database.writeDatabase();
        //Refresh Status Table
        controller.refreshStatusTable();
        ServerMain.execIO.execute(new Server.UpdateStatus(getUser(), getGroup()));
    }

    public void signOut() {
        interrupt = true;
    }

    public void errorMessage(String message) throws IOException {
        out.println(new Message("Error", "", "", message).toString());
    }
}
