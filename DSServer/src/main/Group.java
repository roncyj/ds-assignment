/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author RonCYJ
 */
public class Group {

    private String groupName;
    private ArrayList<String> memberList = new ArrayList();

    public Group(String groupName) {
        this.groupName = groupName;
    }

    public Group(String groupName, String[] memberList) {
        this.groupName = groupName;
        this.memberList.addAll(Arrays.asList(memberList));
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the memberList
     */
    public ArrayList<String> getMemberList() {
        return memberList;
    }

    public String getMemberListStr() {
        String str = "";
        for (int i = 0; i < memberList.size(); i++) {
            if (!memberList.get(i).contains("Anonymous")) {
                str += memberList.get(i) + ";";
            }
        }
        return str;
    }

    /**
     * @param memberList the friendList to add
     */
    public void addMemberList(String memberList) {
        this.memberList.add(memberList);
    }

}
