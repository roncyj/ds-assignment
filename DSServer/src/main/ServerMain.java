/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import main.Server.OfflineMsgHandler;

/**
 *
 * @author RonCYJ
 */
public class ServerMain extends Application {

    public static Server serverClass = new Server();
    public static Thread server = new Thread(serverClass);
    public static FXMLServerController controller;
    public static ExecutorService execIO = Executors.newCachedThreadPool();
    public static OfflineMsgHandler offlineMsgHandler = new OfflineMsgHandler();
    public static Thread offMsgHandler = new Thread(offlineMsgHandler);
    public static Database database = new Database();
    public static int port;

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoad = new FXMLLoader(getClass().getResource("FXMLServer.fxml"));
        Parent root = fxmlLoad.load();
        Scene scene = new Scene(root);
        controller = fxmlLoad.<FXMLServerController>getController();
        stage.setScene(scene);
        stage.show();
        System.out.println("Application Running");

        // Automatic off server when exit the application
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                controller.endServerAction(null);
                Platform.exit();
                System.exit(0);
            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            port = 8080;
        } else {
            try {
                port = Integer.parseInt(args[0]);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
        launch(args);
    }

    public static ArrayList<User> getUser() {
        return serverClass.getUser();
    }

    public static ArrayList<Group> getGroup() {
        return serverClass.getGroup();
    }

    public static int getUserNum(String userName) {
        return serverClass.getUserNum(userName);
    }

    public static int getGroupNum(String groupName) {
        return serverClass.getGroupNum(groupName);
    }
}
