/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.IOException;
import static main.ServerMain.getUser;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import main.Server.IndividualMessage;

/**
 * FXML Controller class
 *
 * @author RonCYJ
 */
public class FXMLServerController implements Initializable {

    @FXML
    public TableView<User> statusTable;
    @FXML
    public TableColumn<User, String> chatName;
    @FXML
    public TableColumn<User, String> chatType;
    @FXML
    public TableColumn<User, String> chatStatus;
    @FXML
    private TextArea chatBox;
    @FXML
    private TextField messageBox;
    public ObservableList<User> chatData = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        chatName.setCellValueFactory(new PropertyValueFactory<>("userName"));
        chatStatus.setCellValueFactory(new PropertyValueFactory<>("status"));

        // Listen for selection changes
        statusTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {
                            chatBox.setText(null);
                        });

        //Auto start server thread
        Thread startServer;
        startServer = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        chatBox.appendText("Auto Start Server in 3 second\n");
                        for (int i = 0; i < 3; i++) {
                            chatBox.appendText((3 - i) + "\n");
                            this.wait(1000);
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(FXMLServerController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                ServerMain.server.start();
            }
        });
        startServer.start();
    }

    @FXML
    private void startServerAction(ActionEvent event) {
        ServerMain.server.start();
    }

    @FXML
    public void endServerAction(ActionEvent event) {
        synchronized (ServerMain.offlineMsgHandler) {
            ServerMain.offlineMsgHandler.notify();
        }
        ServerMain.offlineMsgHandler.interrupt();
        ServerMain.server.stop();
    }

    @FXML
    private void removeUserAction(ActionEvent event) throws IOException {
        if (statusTable.getSelectionModel().selectedItemProperty().getValue() == null) {
            alertDialog("No Selection", "No Item Selected", "Please select a Friend/Group in the table.");
            return;
        }
        getUser().remove(statusTable.getSelectionModel().selectedItemProperty().getValue());
        refreshUserAction(event);
        ServerMain.database.writeDatabase();
    }

    @FXML
    private void sendAction(ActionEvent event) {
        if (statusTable.getSelectionModel().selectedItemProperty().getValue() == null) {
            alertDialog("No Selection", "No Item Selected", "Please select a Friend/Group in the table.");
            return;
        }
        // Get Selected User Name
        String targetName = statusTable.getSelectionModel().getSelectedItem().getUserName();
        // Get the selected user index
        int userNum = ServerMain.serverClass.getUserNum(targetName);
        chatBox.appendText("User Num" + userNum + "   Msg:" + messageBox.getText());
        ServerMain.execIO.execute(new IndividualMessage(ServerMain.getUser().get(userNum), new Message("IndChat", targetName, "Server", messageBox.getText())));
    }

    @FXML
    private void refreshUserAction(ActionEvent event) {
        refreshStatusTable();
    }

    // refresh the table
    public synchronized void refreshStatusTable() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                chatData.clear();
                chatData.addAll(getUser());
                int selectedIndex = statusTable.getSelectionModel()
                        .getSelectedIndex();
                statusTable.setItems(null);
                statusTable.layout();
                statusTable.setItems(chatData);
                statusTable.getSelectionModel().select(selectedIndex);
            }
        });

    }

    //Append text into current chatBox Text Area
    public void updateText(String text) {
        chatBox.appendText(text + "\n");
    }

    // when there are no selection, display error message
    public void alertDialog(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();

        //Deprecated -  org.controlsfx.dialog.Dialogs;
        //Dialogs.create().title("No Selection").masthead("No Item Selected")
        //        .message("Please select a Friend/Group in the table.").showError();
    }
}
