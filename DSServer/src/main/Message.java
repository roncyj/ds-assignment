/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author RonCYJ
 */
public class Message {

    private String type;
    private String destination;
    private String source;
    private String content;

    public Message() {

    }

    public Message(String str) {
        strToObject(str);
    }

    public Message(String type, String destination, String source, String content) {
        this.type = type;
        this.destination = destination;
        this.source = source;
        this.content = content;
    }

    // Convert String into Message Object
    public void strToObject(String inputStr) {
        String[] str = inputStr.split("[;]");
        type = str[0];
        destination = str[1];
        source = str[2];
        content = "";
        for (int i = 3; i < str.length; i++) {
            if (i != (str.length - 1)) {
                content = getContent() + str[i] + ";";
            } else {
                content = getContent() + str[i];
            }
        }
    }

    @Override
    // Convert to message into structered string to be accepted by another client
    public String toString() {
        return getType() + ";" + getDestination() + ";" + getSource() + ";" + getContent();
    }

    public String getType() {
        return type;
    }

    public String getDestination() {
        return destination;
    }

    public String getSource() {
        return source;
    }

    public String getContent() {
        return content;
    }

}
