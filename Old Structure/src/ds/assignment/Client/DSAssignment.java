/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.assignment.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author RonCYJ
 */
public class DSAssignment extends Application {

    public static Socket echoSocket = null;
    public static PrintWriter out = null;
    public static BufferedReader in = null;
    public static FXMLClientController controller;

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoad = new FXMLLoader(getClass().getResource("FXMLClient.fxml"));
        Parent root = fxmlLoad.load();
        Scene scene = new Scene(root);
        controller = fxmlLoad.<FXMLClientController>getController();
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                controller.signOutAction(null);
                Platform.exit();
                System.exit(0);
                
            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
