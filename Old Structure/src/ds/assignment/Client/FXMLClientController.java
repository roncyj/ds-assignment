/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.assignment.Client;

import static ds.assignment.Client.DSAssignment.echoSocket;
import static ds.assignment.Client.DSAssignment.in;
import static ds.assignment.Client.DSAssignment.out;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import static javafx.scene.input.KeyCode.ENTER;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author RonCYJ
 */
public class FXMLClientController implements Initializable {

    @FXML
    private TableView<Chat> statusTable;
    @FXML
    private TableColumn<Chat, String> chatName;
    @FXML
    private TableColumn<Chat, String> chatType;
    @FXML
    private TableColumn<Chat, String> chatStatus;
    @FXML
    private Button deleteFriendBtn;
    @FXML
    private Button newGroupBtn;
    @FXML
    private TextArea chatBox;
    @FXML
    private TextField messageBox;
    @FXML
    private AnchorPane loginPane;
    @FXML
    private AnchorPane messagingPane;
    @FXML
    private TextField userName;
    @FXML
    private TextField userPassword;
    @FXML
    private Button anonLogin;
    @FXML
    private Button userLogin;
    @FXML
    private Label userNameLabel;
    private ObservableList<Chat> chatData = FXCollections.observableArrayList();
    Thread receiveTask;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        chatName.setCellValueFactory(new PropertyValueFactory<Chat, String>("name"));
        chatType.setCellValueFactory(new PropertyValueFactory<Chat, String>("type"));
        chatStatus.setCellValueFactory(new PropertyValueFactory<Chat, String>("status"));

        // Listen for selection changes
        statusTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {
                            chatBox.setText(null);
                        });
        messageBox.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
            switch (e.getCode()) {
                case ENTER: {
                    if (e.isShiftDown() && messageBox.getText().equals("")) {
                        messageBox.appendText("\n");
                    } else {
                        sendAction(null);
                    }
                    break;
                }
            }
        });
        receiveTask = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String newLine;
                    while ((newLine = DSAssignment.in.readLine()) != null) {
                        chatBox.appendText(newLine + "\n");
                    };
                } catch (IOException ex) {
                    Logger.getLogger(FXMLClientController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        });
    }

    @FXML
    private void sendAction(ActionEvent event) {
        if (messageBox.getText().equalsIgnoreCase("")) {
            System.out.println("New: " + messageBox.getText());
            out.println("U1;user1;" + messageBox.getText());
            messageBox.setText(null);
            if (statusTable.getSelectionModel().selectedItemProperty().getValue() == null) {
                alertDialog("No Selection", "No Item Selected", "Please select a Friend/Group in the table.");
                return;
            }
        }
    }

    @FXML
    private void sendFileAction(ActionEvent event) {
        out.println("U3;File");
        if (statusTable.getSelectionModel().selectedItemProperty().getValue() == null) {
            alertDialog("No Selection", "No Item Selected", "Please select a Friend/Group in the table.");
            return;
        }
    }

    @FXML
    private void newFriendAction(ActionEvent event) {
        out.println("U4;newFriend");
    }

    @FXML
    private void deleteFriendAction(ActionEvent event) {
        out.println("U5;deleteFriend");
        if (statusTable.getSelectionModel().selectedItemProperty().getValue() == null) {
            alertDialog("No Selection", "No Item Selected", "Please select a Friend/Group in the table.");
            return;
        }
    }

    @FXML
    private void newGroupAction(ActionEvent event) {
        out.println("U6;Newgroup");
    }

    // refresh the table
    public void refreshStatusTable() {
        chatData.clear();
        //chatData.addAll(ItemFunction.data);
        int selectedIndex = statusTable.getSelectionModel()
                .getSelectedIndex();
        statusTable.setItems(null);
        statusTable.layout();
        statusTable.setItems(chatData);
        statusTable.getSelectionModel().select(selectedIndex);
    }

    // when there are no selection, display error message
    public void alertDialog(String title, String header, String content) {
//        Alert alert = new Alert(AlertType.ERROR);
//        alert.setTitle(title);
//        alert.setHeaderText(header);
//        alert.setContentText(content);
//        alert.showAndWait();

        //Deprecated -  org.controlsfx.dialog.Dialogs;
        //Dialogs.create().title("No Selection").masthead("No Item Selected")
        //        .message("Please select a Friend/Group in the table.").showError();
    }

    @FXML
    public void signOutAction(ActionEvent event) {
        out.println("U7;signOut");
        loginPane.setVisible(true);
        messagingPane.setVisible(false);
    }

    @FXML
    private void anonLoginAction(ActionEvent event) {
        connect2Server();
        loginPane.setVisible(false);
        messagingPane.setVisible(true);
        out.println("U0;Anon");
    }

    @FXML
    private void userLoginAction(ActionEvent event) throws IOException {
        connect2Server();
        out.println("U0;" + userName.getText() + ";" + userPassword.getText());
        if (in.readLine().equalsIgnoreCase("A0;Success")) {
            loginPane.setVisible(false);
            messagingPane.setVisible(true);
            userNameLabel.setText(userName.getText());
            receiveTask.start();
        } else {
            alertDialog("Access Denied", "Wrong User Name/Password", "Please enter the correct UserName/Password");
        }
    }

    public void connect2Server() {
        try {
            echoSocket = new Socket("RONCYJ-HP", 8080);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host taranis");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couln't get I/O for the connection to the name you used for your socket.");
            System.exit(1);
        }
    }
}
