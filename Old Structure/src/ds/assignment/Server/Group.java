/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.assignment.Server;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author RonCYJ
 */
public class Group {

    private String groupName;
    private ArrayList<String> memberList = new ArrayList();
    
    public Group (String groupName, String[] memberList){
        this.groupName = groupName;
        this.memberList.addAll(Arrays.asList(memberList));
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the memberList
     */
    public ArrayList<String> getMemberList() {
        return memberList;
    }

    /**
     * @param memberList the friendList to add
     */
    public void addGroupList(String memberList) {
        this.memberList.add(memberList);
    }

    /**
     * @param groupList the friendList to delete
     */
    public void deleteGroupList(String memberList) {
        this.memberList.remove(memberList);
    }
}
