/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.assignment.Server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author RonCYJ
 */
public class User {

    private String userName;
    private String userPassword;
    private String status = "Offline";
    private ArrayList<String> friendList = new ArrayList();
    private ArrayList<String> groupList = new ArrayList();
    private Socket socket = new Socket();

    public User(String userName, String userPassword, String[] friendList, String[] groupList) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.friendList.addAll(Arrays.asList(friendList));
        this.groupList.addAll(Arrays.asList(groupList));
    }

    public User(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword the userPassword to set
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * @return the status
     */
    public String isStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the friendList
     */
    public ArrayList<String> getFriendList() {
        return friendList;
    }

    /**
     * @param friendList the friendList to add
     */
    public void addFriendList(String friendName) {
        this.friendList.add(friendName);
    }

    /**
     * @param friendList the friendList to delete
     */
    public void deleteFriendList(String friendName) {
        this.friendList.remove(friendName);
    }

    /**
     * @return the groupList
     */
    public ArrayList<String> getGroupList() {
        return groupList;
    }

    /**
     * @param groupList the friendList to add
     */
    public void addGroupList(String groupName) {
        this.groupList.add(groupName);
    }

    /**
     * @param groupList the friendList to delete
     */
    public void deleteGroupList(String groupName) {
        this.groupList.remove(groupName);
    }

    /**
     * @return the socket
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * @param socket the socket to set
     */
    public void setSocket(Socket socket) {
        this.socket = socket;
    }

}
