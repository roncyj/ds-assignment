/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.assignment.Server;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author RonCYJ
 */
public class ServerMain extends Application {

    public static Server serverClass = new Server(8080);
    public static Thread server = new Thread(serverClass);
    public static FXMLServerController controller;

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoad = new FXMLLoader(getClass().getResource("FXMLServer.fxml"));
        Parent root = fxmlLoad.load();
        Scene scene = new Scene(root);
        controller = fxmlLoad.<FXMLServerController>getController();
        stage.setScene(scene);
        stage.show();
        System.out.println("Application Running");
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public static ArrayList<User> getUser() {
        return serverClass.getUser();
    }

    public static ArrayList<User> getChatGroup() {
        return serverClass.getChatGroup();
    }

    public static int getUserNum(String userName) {
        return serverClass.getUserNum(userName);
    }

}