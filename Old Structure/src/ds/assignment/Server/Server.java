/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.assignment.Server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RonCYJ
 */
public class Server implements Runnable {

    ExecutorService exec = Executors.newCachedThreadPool();
    private ArrayList<User> user = new ArrayList();
    private ArrayList<User> onlineUser = new ArrayList();
    private ArrayList chatGroup = new ArrayList();
    private int portNumber;
    public ServerSocket serverSocket;
    boolean interrupt = false;
    int numOfConnection = 0;

    public Server(int portNumber) {
        this.portNumber = portNumber;
    }

    @Override
    public void run() {
        user.add(new User("user1", "user", new String[]{"user2", "user3"}, new String[]{"Hey", "Yo"}));
        user.add(new User("user2", "user", new String[]{"user2", "user3"}, new String[]{"Hey", "Yo"}));
        user.add(new User("user3", "user", new String[]{"user2", "user3"}, new String[]{"Hey", "Yo"}));
        ServerMain.controller.updateText("Added User");
        try {
            serverSocket = new ServerSocket(portNumber);
            
            System.out.println("Server Running");
            ServerMain.controller.updateText("Server Running");
            while (!interrupt) {
                numOfConnection++;
                exec.execute(new ServerThread(serverSocket.accept(), numOfConnection));
                ServerMain.controller.updateText("User Login");
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the user
     */
    public ArrayList<User> getUser() {
        return user;
    }

    public int getUserNum(String name) {
        int i = 0;
        boolean flag = false;
        for (; i < user.size(); i++) {
            if (user.get(i).getUserName().equalsIgnoreCase(name)) {
                flag = true;
                break;
            }
        }
        if (flag) {
            return i;
        } else {
            return -1;
        }
    }

    /**
     * @param user the user to set
     */
    public synchronized void setUser(ArrayList user) {
        this.user = user;
    }

    /**
     * @return the chatGroup
     */
    public ArrayList getChatGroup() {
        return chatGroup;
    }

    /**
     * @param chatGroup the chatGroup to set
     */
    public void setChatGroup(ArrayList chatGroup) {
        this.chatGroup = chatGroup;
    }

    /**
     * @return the portNumber
     */
    public int getPortNumber() {
        return portNumber;
    }

    /**
     * @param portNumber the portNumber to set
     */
    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public void interrupt() {
        interrupt = true;
    }

    static class IndividualMessage implements Runnable {

        User destination;
        User source;
        String message;
        PrintWriter out;

        public IndividualMessage(User destination, User source, String message) {
            this.destination = destination;
            this.source = source;
            this.message = message;
        }

        @Override
        public void run() {
            System.out.println("Sending Message");
            System.out.println(destination.getUserName() + ";" + source.getUserName() + ";" + message);
            try {
                out = new PrintWriter(destination.getSocket().getOutputStream(), true);
                out.println(message);
            } catch (IOException ex) {
                Logger.getLogger(IndividualMessage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
//
//    static class GroupMessage implements Runnable {
//
//        Group destination;
//        User source;
//        String message;
//        PrintWriter out;
//
//        public GroupMessage(Group destination, User source, String message) {
//            this.destination = destination;
//            this.source = source;
//            this.message = message;
//        }
//
//        @Override
//        public void run() {
//            System.out.println("Sending Message");
//            System.out.println("" + ";" + source.getUserName() + ";" + message);
//            try {
//                for (int i = 0; i < destination.getMemberList().size(); i++) {
//                    
//                    out = new PrintWriter(, true);
//                    out.println(message);
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(GroupMessage.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
}
