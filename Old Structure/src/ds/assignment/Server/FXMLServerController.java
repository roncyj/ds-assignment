/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.assignment.Server;

import static ds.assignment.Server.ServerMain.getUser;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import org.controlsfx.dialog.Dialogs;

/**
 * FXML Controller class
 *
 * @author RonCYJ
 */
public class FXMLServerController implements Initializable {

    @FXML
    public TableView<User> statusTable;
    @FXML
    public TableColumn<User, String> chatName;
    @FXML
    public TableColumn<User, String> chatType;
    @FXML
    public TableColumn<User, String> chatStatus;
    @FXML
    private TextArea chatBox;
    @FXML
    private TextArea messageBox;
    public ObservableList<User> chatData = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        chatName.setCellValueFactory(new PropertyValueFactory<User, String>("userName"));
        chatStatus.setCellValueFactory(new PropertyValueFactory<User, String>("status"));

        // Listen for selection changes
        statusTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {
                            chatBox.setText(null);
                        });
//        messageBox.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
//            switch (e.getCode()) {
//                case ENTER: {
//                    if (!e.isShiftDown() && messageBox.getText().equals("")) {
//                        messageBox.appendText("\n");
//                    } else {
//                        sendAction(null);
//                    }
//                    break;
//                }
//            }
//        });
    }

    @FXML
    private void startServerAction(ActionEvent event) {
        ServerMain.server.start();
    }

    @FXML
    private void endServerAction(ActionEvent event) {
        ServerMain.server.stop();
    }

    @FXML
    private void removeUserAction(ActionEvent event) {
        if (statusTable.getSelectionModel().selectedItemProperty().getValue() == null) {
            noItemSelected();
            return;
        }
    }

    @FXML
    private void sendAction(ActionEvent event) {
    }

    @FXML
    private void sendFileAction(ActionEvent event) {
        refreshStatusTable();
    }

    // refresh the table
    public synchronized void refreshStatusTable() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                chatData.clear();
                chatData.addAll(getUser());
                int selectedIndex = statusTable.getSelectionModel()
                        .getSelectedIndex();
                statusTable.setItems(null);
                statusTable.layout();
                statusTable.setItems(chatData);
                statusTable.getSelectionModel().select(selectedIndex);
            }
        });

    }

    // when there are no selection, display error message
    public void noItemSelected() {
        Dialogs.create().title("No Selection").masthead("No Item Selected")
                .message("Please select a Friend/Group in the table.").showError();
    }

    public void updateText(String text) {
        chatBox.appendText(text + "\n");
    }
}
