/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.assignment.Server;

import static ds.assignment.Server.ServerMain.controller;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import static ds.assignment.Server.ServerMain.getUser;
import static ds.assignment.Server.ServerMain.getUserNum;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author RonCYJ
 */
public class ServerThread implements Runnable {

    private Socket clientSocket = null;
    int connectionNum;
    String name;
    int userNum = 0;
    boolean flag = false;
    boolean anonLoginFlag = false;
    boolean interrupt = false;

    PrintWriter out;

    public ServerThread(Socket socket, int connectionNum) {
        clientSocket = socket;
        this.connectionNum = connectionNum;
    }

    @Override
    public void run() {
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String inputLine;
            String[] inputArray;
            System.out.println("Connected to Client on Connection " + connectionNum);
            System.out.println("Waiting for authentication");
            inputArray = in.readLine().split("[;]");
            controller.updateText(Arrays.toString(inputArray));
            if (inputArray[0].equalsIgnoreCase("U0")) {
                if (inputArray[1].equalsIgnoreCase("Anon")) {
                    controller.updateText("Anonymous Login");
                    anonLoginFlag = true;
                } else if ((userNum = getUserNum(inputArray[1])) == -1) {
                    System.out.println("Can't find User name");
                    out.println("A0;Denied");
                    controller.updateText("Can't find User Name");
                    return;
                }
                if (anonLoginFlag) {
                    getUser().add(new User("Anonymous1"));
                    userNum = getUser().size() - 1;
                }
                name = getUser().get(userNum).getUserName();
                getUser().get(userNum).setSocket(clientSocket);
                if ((inputArray[1].equalsIgnoreCase(name)
                        && inputArray[2].equalsIgnoreCase(getUser().get(userNum).getUserPassword())) || anonLoginFlag) {
                    //User is connected to server
                    out.println("A0;Success");
                    controller.updateText("User is Connected to server");
                    System.out.println(name + " Connected");
                    getUser().get(userNum).setStatus("Online");
                    controller.refreshStatusTable();
                    while ((inputLine = in.readLine()) != null) {
                        inputArray = inputLine.split("[;]");
                        switch (inputArray[0]) {
                            case "U1":
                                //Process individual message
                                controller.updateText("Sending Individual message");
                                ExecutorService exec = Executors.newCachedThreadPool();
                                exec.execute(new Server.IndividualMessage(getUser().get(getUserNum(inputArray[1])),getUser().get(userNum), inputArray[2]));
                                if (inputArray.length > 2) {
                                    individualMsg(inputArray[1], inputArray[2]);
                                }
                                break;
                            case "U2":
                                //Process group message
                                controller.updateText("Sending Group Message");
                                if (inputArray.length > 2) {
                                    groupMsg(inputArray[1], inputArray[2]);
                                }
                                break;
                            case "U3":
                                //File Transfer
                                controller.updateText("File Transfer");
                                fileTransfer(inputArray[1]);
                                break;
                            case "U4":
                                //New Friend
                                controller.updateText("New Friend");
                                newFriend(inputArray[1]);
                                break;
                            case "U5":
                                //Delete Friend
                                controller.updateText("Delete Friend");
                                deleteFriend(inputArray[1]);
                                break;
                            case "U6":
                                //New Group
                                controller.updateText("New Group");
                                String[] memberList = new String[inputArray.length - 2];
                                for (int i = 0; i < memberList.length; i++) {
                                    memberList[i] = inputArray[i + 2];
                                }
                                newGroup(inputArray[1], memberList);
                                break;
                            case "U7":
                                //Sign Out
                                controller.updateText("Sign Out");
                                signOut();
                                break;
                            default:
                                errorMessage("Input Error");
                        }
                        if (interrupt) {
                            break;
                        }
                    }
                } else {
                    System.out.println("Incorrect Password");
                    out.println("9;Incorrect Password");
                    controller.updateText("Incorrect Password");
                }
            } else {
                System.out.println("Access Denied");
                out.println("9;Access Denied");
            }
            System.out.println("Out");
            controller.updateText("User is disconnect from server");
            getUser().get(userNum).setStatus("Offline");
            controller.refreshStatusTable();
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port " + "7" + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }

    public void individualMsg(String receiver, String message) {

    }

    public void groupMsg(String groupName, String message) {

    }

    public Socket fileTransfer(String receiver) {
        Socket receiverSocket = new Socket();
        return receiverSocket;
    }

    public void newFriend(String userName) {

    }

    public void deleteFriend(String userName) {

    }

    public void newGroup(String groupName, String[] userName) {

    }

    public void signOut() {
        interrupt = true;
    }

    public void errorMessage(String message) {
        out.println("9;" + message);
    }
}
