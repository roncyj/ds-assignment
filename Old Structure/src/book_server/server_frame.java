/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package book_server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author wongwengkeong
 */
public class server_frame extends javax.swing.JFrame {

    /**
     * Creates new form server_frame
     */
    ArrayList<String> users;
    ArrayList clientOutputStreams;
    Boolean isAdminConnected = false;

    public server_frame() {
        initComponents();
    }

    PrintWriter writer;

    public class ServerStart implements Runnable {

        @Override
        public void run() {
            clientOutputStreams = new ArrayList();
            users = new ArrayList();

            try {
                ServerSocket serverSock = new ServerSocket(2222);

                while (true) {

                    Socket clientSock = serverSock.accept();
                    writer = new PrintWriter(clientSock.getOutputStream());
                    clientOutputStreams.add(writer);
                    //updateData();
                    Thread listener = new Thread(new ClientHandler(clientSock, writer));
                    listener.start();

                }
            } catch (Exception ex) {
                server_Area.append("Error making a connection. \n");
            }
        }
    }

    BufferedReader br = null;

    public void updateData() {
        Iterator it = clientOutputStreams.iterator();

        while (it.hasNext()) {
            try {
                PrintWriter writer = (PrintWriter) it.next();
                String sCurrentLine;

                br = new BufferedReader(new FileReader("C:\\Users\\wongwengkeong\\Documents\\NetBeansProjects\\Book_Server\\src\\book_server\\database.txt"));

                while ((sCurrentLine = br.readLine()) != null) {
                    server_Area.append(sCurrentLine + "\n");
                    writer.println(sCurrentLine);
                    writer.flush();

                }
                       //writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (br != null) {
                        br.close();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }

    public void voiceOut(String message) {
        Iterator it = clientOutputStreams.iterator();

        while (it.hasNext()) {
            try {
                PrintWriter writer = (PrintWriter) it.next();
                writer.println(message);

                server_Area.append("Sending: " + message + "\n");
                server_Area.setCaretPosition(server_Area.getDocument().getLength());

            } catch (Exception ex) {
                server_Area.append("Error telling everyone. \n");
            }
            writer.flush();
        }

    }

    public void userAdd(String data) {
        String message, add = ": :Connect", done = "Server: :Done", name = data;
        //ta_chat.append("Before " + name + " added. \n");
        users.add(name);
        // ta_chat.append("After " + name + " added. \n");
//        String[] tempList = new String[(users.size())];
//        users.toArray(tempList);
//
//        for (String token:tempList) 
//        {
//            message = (token + add);
//            tellEveryone(message);
//        }
//        tellEveryone(done);
    }

    public void userRemove(String data) {
        String message, add = ": :Connect", done = "Server: :Done", name = data;
        users.remove(name);
        if (name.equals("Admin")) {
            isAdminConnected = false;
        }

//        String[] tempList = new String[(users.size())];
//        users.toArray(tempList);
//
//        for (String token:tempList) 
//        {
//            message = (token + add);
//            tellEveryone(message);
//        }
//        tellEveryone(done);
    }

    public class ClientHandler implements Runnable {

        BufferedReader reader;
        Socket sock;
        PrintWriter client;

        public ClientHandler(Socket clientSocket, PrintWriter user) {
            client = user;
            try {
                sock = clientSocket;
                InputStreamReader isReader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(isReader);
            } catch (Exception ex) {
                //ta_chat.append("Unexpected error... \n");
            }

        }

        @Override
        public void run() {
            String message, connect = "Connect", disconnect = "Disconnect", borrow = "Borrow", add = "Add", edit = "Edit", delete = "Delete", admin = "Admin", guest = "Guest";
            String[] data;

            try {
                while ((message = reader.readLine()) != null) {
                    server_Area.append("Received: " + message + "\n");
                    data = message.split(":");

                    for (String token : data) {
                        server_Area.append(token + "\n");
                    }

                    if (data[0].equals(admin)) {
                        if (data[1].equals(admin) && data[2].equals("admin123")) {
                            if (!isAdminConnected) {
                                isAdminConnected = true;
                                voiceOut((admin) + ":" + "Login:" + "Successful");
                                updateData();
                            } else {
                                voiceOut((admin) + ":" + "Login:" + "Error");
                            }
                        } else {
                            voiceOut((admin) + ":" + "Login:" + "Unsuccessful");
                        }

                    } else if (data[0].equals(guest)) {
                        voiceOut((guest) + ":" + "Login:" + "Successful");
                        updateData();
                    } else if (data[0].equals(connect)) {
                        userAdd(data[1]);
                        System.out.println(users.size());
                    } else if (data[0].equals(disconnect)) {
                        userRemove(data[1]);
                        System.out.println(users.size());
                    } else {
                        server_Area.append("No Conditions were met. \n");
                    }
                }
            } catch (Exception ex) {
                server_Area.append("Lost a connection. \n");
                ex.printStackTrace();
                clientOutputStreams.remove(client);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        server_Area = new javax.swing.JTextArea();
        startEvent = new javax.swing.JButton();
        endEvent = new javax.swing.JButton();
        clearEvent = new javax.swing.JButton();
        onlineusersEvent = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        server_Area.setColumns(20);
        server_Area.setRows(5);
        jScrollPane1.setViewportView(server_Area);

        startEvent.setText("Start");
        startEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startEventActionPerformed(evt);
            }
        });

        endEvent.setText("End");
        endEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                endEventActionPerformed(evt);
            }
        });

        clearEvent.setText("Clear");
        clearEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearEventActionPerformed(evt);
            }
        });

        onlineusersEvent.setText("Online Users");
        onlineusersEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onlineusersEventActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 483, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(startEvent, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                            .addComponent(endEvent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(onlineusersEvent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(clearEvent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(43, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startEvent)
                    .addComponent(clearEvent))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(onlineusersEvent)
                    .addComponent(endEvent))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void endEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_endEventActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_endEventActionPerformed

    private void startEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startEventActionPerformed
        // TODO add your handling code here:
        Thread starter = new Thread(new ServerStart());
        starter.start();
        server_Area.append("Server started...\n");
    }//GEN-LAST:event_startEventActionPerformed

    private void clearEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearEventActionPerformed
        // TODO add your handling code here:
        server_Area.setText("");
    }//GEN-LAST:event_clearEventActionPerformed

    private void onlineusersEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onlineusersEventActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_onlineusersEventActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(server_frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(server_frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(server_frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(server_frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new server_frame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clearEvent;
    private javax.swing.JButton endEvent;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton onlineusersEvent;
    private javax.swing.JTextArea server_Area;
    private javax.swing.JButton startEvent;
    // End of variables declaration//GEN-END:variables
}
