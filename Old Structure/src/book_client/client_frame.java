/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package book_client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author wongwengkeong
 */
public class client_frame extends javax.swing.JFrame {

    /**
     * Creates new form client_frame
     */
    
    Boolean isAdmin=false;
    Boolean isConnected = false;
    String username;
    String password;
    int port=2222;
    
    Socket sock;
    BufferedReader reader;
    PrintWriter writer;
    
    public client_frame() {
        initComponents();
    }
    
    public void ListenThread() 
       {
            Thread IncomingReader = new Thread(new IncomingReader());
            IncomingReader.start();
       }
    
    public void clearTable()
    {
        DefaultTableModel model = (DefaultTableModel) item_Table.getModel();
        model.setRowCount(0);
    }
    
     public class IncomingReader implements Runnable
    {
        @Override
        public void run() 
        {
            boolean update=false;
            String[] data;
            String stream, connect = "Connect", disconnect = "Disconnect", borrow="Borrow", add="Add", edit="Edit"
                    , delete="Delete", admin="Admin", guest="Guest" ;
            
            try 
            {

                while ((stream = reader.readLine()) != null) 
                {
                    DefaultTableModel model = (DefaultTableModel) item_Table.getModel();
                    if(update==true)
                    {
                        clearTable();
                    }
                     data = stream.split(":");

                     if(data[0].equals(admin)&&isAdmin&&!isConnected)
                     {
                         if(data[2].equals("Successful"))
                         {
                             
                             JFrame frame= new JFrame();
                            
                              writer.println("Connect:"+username+":to the Server");
                              writer.flush();
                              JOptionPane.showMessageDialog(frame, "Connected to server at Port "+port);
                                //borrowEvent.setEnabled(true);
                                addEvent.setEnabled(true);
                                editEvent.setEnabled(true);
                                deleteEvent.setEnabled(true);
                                isConnected = true; 
                               
                         }
                         else if(data[2].equals("Unsucessful"))
                         {
                             JFrame frame= new JFrame();
                             isAdmin=false;
                             isConnected = false; 
                                writer.close();
                                reader.close();
                                sock.close();
                                JOptionPane.showMessageDialog(frame,"Wrong Password and Username","Connection Fail",JOptionPane.ERROR_MESSAGE);
                                 username_Text.setEditable(true);
                                password_Text.setEditable(true);
                         }
                         else //if error eg: other ppl logged in
                         {
                             JFrame frame= new JFrame();
                             isAdmin=false;
                             isConnected = false; 
                                writer.close();
                                reader.close();
                                sock.close();
                                JOptionPane.showMessageDialog(frame,"Someone is already logged in","Connection Fail",JOptionPane.ERROR_MESSAGE);
                                 username_Text.setEditable(true);
                                password_Text.setEditable(true);
                         }
                         
                     }
                     else if(data[0].equals(guest)&&!isAdmin&&!isConnected)
                     {
                         JFrame frame= new JFrame();
                         JOptionPane.showMessageDialog(frame, "Connected to server at Port "+port);
                         writer.println("Connect:"+username+":to the Server");
                         writer.flush();
                         borrowEvent.setEnabled(true);
                         isConnected = true; 
                     }
                     else
                     {
                        String itemcode = data[0];
                        String title= data[1];
                        String author=data[2];
                        String publisher=data[3];
                        int quantity= Integer.parseInt(data[4]);
                        String status=data[5];
                        String borrowdate=data[6];
                        String retrievaldate=data[7];
                        String username=data[8];
                        
                        int row=model.getRowCount();
                        for(int i=0;i<row;i++)
                        {
                            
                            String temp_itemcode=model.getValueAt(0, i).toString();
                            if(itemcode.equals(temp_itemcode))
                            {
                                model.removeRow(i);
                            }
                        }
                        
    
                        model.addRow(new Object[] {itemcode, title, author, publisher, quantity, status, borrowdate, retrievaldate, username});
                        update=false;
                     }
                }
                update=true;
                
           }catch(Exception ex) { }
        }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        item_Table = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        username_Text = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        password_Text = new javax.swing.JTextField();
        adminEvent = new javax.swing.JButton();
        disconnectEvent = new javax.swing.JButton();
        guestEvent = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        borrowEvent = new javax.swing.JButton();
        editEvent = new javax.swing.JButton();
        deleteEvent = new javax.swing.JButton();
        addEvent = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        item_Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ITEMCODE", "TITLE", "AUTHOR", "PUBLISHER", "QUANTITY", "STATUS", "BORROW DATE", "RETRIEVAL DATE", "USERNAME"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(item_Table);

        jLabel1.setText("Username:");

        jLabel2.setText("Password:");

        adminEvent.setText("Admin Login");
        adminEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adminEventActionPerformed(evt);
            }
        });

        disconnectEvent.setText("Disconnect");
        disconnectEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                disconnectEventActionPerformed(evt);
            }
        });

        guestEvent.setText("Anonymous Login");
        guestEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guestEventActionPerformed(evt);
            }
        });

        jLabel3.setText("Search:");

        borrowEvent.setText("Borrow");
        borrowEvent.setEnabled(false);
        borrowEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                borrowEventActionPerformed(evt);
            }
        });

        editEvent.setText("Edit");
        editEvent.setEnabled(false);
        editEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editEventActionPerformed(evt);
            }
        });

        deleteEvent.setText("Delete");
        deleteEvent.setEnabled(false);
        deleteEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteEventActionPerformed(evt);
            }
        });

        addEvent.setText("Add");
        addEvent.setEnabled(false);
        addEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addEventActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(128, 128, 128)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(guestEvent)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(username_Text, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(password_Text, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(disconnectEvent, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                            .addComponent(adminEvent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(borrowEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(addEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(92, 92, 92)
                                .addComponent(editEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(110, 110, 110)
                                .addComponent(deleteEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1))
                        .addGap(18, 18, 18)))
                .addGap(106, 106, 106))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(username_Text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(password_Text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(adminEvent))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(disconnectEvent)
                    .addComponent(guestEvent))
                .addGap(69, 69, 69)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(borrowEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deleteEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void guestEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guestEventActionPerformed
        // TODO add your handling code here:
        //tf_username.setText("");
        if (isConnected == false) 
        {
             JFrame frame= new JFrame();
            String anon="anon";
            Random generator = new Random(); 
            int i = generator.nextInt(999) + 1;
            int port=2222;
            String is=String.valueOf(i);
            anon=anon.concat(is);
            username=anon;
            
            username_Text.setText(anon);
            password_Text.setText("");
            password_Text.setEditable(false);
            username_Text.setEditable(false);
   
            try 
            {
                sock = new Socket("localhost", port);
                InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(streamreader);
                writer = new PrintWriter(sock.getOutputStream());
               // writer.println(anon + ":has connected.:Connect");
                //writer.println("Connect:"+anon+":to the Server");
                writer.println("Guest:"+anon+":to the Server");
                writer.flush(); 
             
             
                
            } 
            catch (Exception ex) 
            {
                 clearTable();
                 JOptionPane.showMessageDialog(frame,"Unable to connect to the server","Connection Fail",JOptionPane.ERROR_MESSAGE);
                username_Text.setEditable(true);
            }
            
            ListenThread();
           // UpdateThread();
            
          
            
        } 
        else if (isConnected == true) 
        {
              JFrame frame= new JFrame();

                JOptionPane.showMessageDialog(frame,"You are already connected","Connection Fail",JOptionPane.ERROR_MESSAGE);


            //ta_chat.append("You are already connected. \n");
        }        
    }//GEN-LAST:event_guestEventActionPerformed

   public void Disconnect() throws IOException 
    { JFrame frame= new JFrame();
        if(isConnected==true)
        {
       
       
        JOptionPane.showMessageDialog(frame, "You have disconnected from the server");
        writer.println("Disconnect:"+username+":to the Server");
            
      
        clearTable();
        isAdmin=false;
        isConnected = false;
        password_Text.setEditable(true);
        username_Text.setEditable(true); 
        borrowEvent.setEnabled(false);
            addEvent.setEnabled(false);
            editEvent.setEnabled(false);
            deleteEvent.setEnabled(false);
            writer.close();
            reader.close();
            sock.close();        
        }
        else
        {
            JOptionPane.showMessageDialog(frame,"You are not Connected","Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void disconnectEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_disconnectEventActionPerformed
        try {
            // TODO add your handling code here:
            Disconnect();
        } catch (IOException ex) {
            Logger.getLogger(client_frame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_disconnectEventActionPerformed

    private void adminEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adminEventActionPerformed
        // TODO add your handling code here:
        if (isConnected == false) 
        {
             JFrame frame= new JFrame();

             username=username_Text.getText();
             password=password_Text.getText();
             
            username_Text.setEditable(false);
            password_Text.setEditable(false);
   
            try 
            {
                sock = new Socket("localhost", port);
                InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(streamreader);
                writer = new PrintWriter(sock.getOutputStream());
                writer.println("Admin:"+username+":"+password);

                writer.flush(); 

                isAdmin=true;
            } 
            catch (Exception ex) 
            {
                 clearTable();
                 JOptionPane.showMessageDialog(frame,"Unable to connect to the server","Connection Fail",JOptionPane.ERROR_MESSAGE);
                username_Text.setEditable(true);
            }
            
            ListenThread();
           // UpdateThread();
            
          
            
        } 
        else if (isConnected == true) 
        {
              JFrame frame= new JFrame();

                JOptionPane.showMessageDialog(frame,"You are already connected","Connection Fail",JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_adminEventActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            // TODO add your handling code here:
            if(isConnected)
            Disconnect();
        } catch (IOException ex) {
            Logger.getLogger(client_frame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_formWindowClosing

    private void borrowEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_borrowEventActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_borrowEventActionPerformed

    private void addEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addEventActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addEventActionPerformed

    private void editEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editEventActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editEventActionPerformed

    private void deleteEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteEventActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteEventActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(client_frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(client_frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(client_frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(client_frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new client_frame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addEvent;
    private javax.swing.JButton adminEvent;
    private javax.swing.JButton borrowEvent;
    private javax.swing.JButton deleteEvent;
    private javax.swing.JButton disconnectEvent;
    private javax.swing.JButton editEvent;
    private javax.swing.JButton guestEvent;
    private javax.swing.JTable item_Table;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField password_Text;
    private javax.swing.JTextField username_Text;
    // End of variables declaration//GEN-END:variables
}
