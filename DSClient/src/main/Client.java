/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import static main.ClientMain.controller;
import static main.ClientMain.in;

/**
 *
 * @author RonCYJ
 */
public class Client {

    public static ArrayList<Name> nameList = new ArrayList();
    public static ExecutorService exec = Executors.newCachedThreadPool();
    public static Database database = new Database();

    // Get the name index from the Name List
    public static int getListNum(String name) {
        int i = 0;
        boolean foundFlag = false;
        for (; i < nameList.size(); i++) {
            String listName = nameList.get(i).getName();
            if (listName.equalsIgnoreCase(name)) {
                //Return index if found
                return i;
            }
        }
        return -1;
    }

    // Runnable class thread to update Name List Status
    static class UpdateStatus implements Runnable {

        String[] msg;

        public UpdateStatus(String[] msg) {
            this.msg = msg;
        }

        @Override
        public void run() {
            try {
                nameList = new ArrayList<>();
                for (int i = 0; i < msg.length; i = i + 3) {
                    nameList.add(new Name(msg[i], msg[i + 1], msg[i + 2]));
                }
                controller.refreshStatusTable(nameList);
                database.checkHistFolder(controller.userNameLabel.getText());
            } catch (IOException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    // Receiver thread that wait for any incoming message
    static class Receiver implements Runnable {

        @Override
        public void run() {
            while (true) {
                try {
                    //Pass String message into Object Mssage
                    Message inMsg = new Message(in.readLine());
                    int listNum; //To get the tarxgeted name index
                    boolean selectionFlag;
                    System.out.println(inMsg.toString());
                    selectionFlag = (controller.statusTable.getSelectionModel().getSelectedItem() == null);
                    switch (inMsg.getType()) {
                        case "Status":
                            //Update Client friend status
                            System.out.println(inMsg.getContent());
                            exec.execute(new Client.UpdateStatus(inMsg.getContent().split("[;]")));
                            break;

                        case "IndChat":
                            individualChat(inMsg, selectionFlag);
                            break;

                        case "GroupChat":
                            groupChat(inMsg, selectionFlag);
                            break;

                        case "SendFile":
                            sendFile(inMsg, selectionFlag);
                            break;
                        case "SendGroupFile":
                            sendGroupFile(inMsg, selectionFlag);
                            break;

                        case "NewGroup":
                            synchronized (this) {
                                this.wait(250);
                            }
                            break;
                    }
                } catch (Exception ex) {
                    try {
                        if (ex.getMessage().equalsIgnoreCase("Connection reset")) {
                            ClientMain.controller.conn2ServerFlag = false;
                            if (ClientMain.controller.anonymousFlag) {
                                ClientMain.controller.anonLoginAction(null);
                            } else {
                                ClientMain.controller.userLoginAction(null);
                            }
                        }
                    } catch (IOException | ClassNotFoundException | InterruptedException ex1) {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }
            }

        }

        public void individualChat(Message inMsg, boolean selectionFlag) throws Exception {
            int listNum;
            // Receive Individual Chat Messages
            // Find the Source index from the list
            if (inMsg.getSource().equalsIgnoreCase("Server")) {
                controller.chatBox.appendText("||---------Message From Server-----------\n" + inMsg.getContent() + "\n||---------End of Message-----------");
            } else if ((listNum = Client.getListNum(inMsg.getSource())) != -1) {
                String decryptedMsg = EncryptingAlgo.decrypt(inMsg.getContent());
                // Add the message to the source name Object
                nameList.get(listNum).addMessage(inMsg.getSource() + " : " + decryptedMsg);
                database.writeChatHist(inMsg.getSource(), listNum, inMsg.getSource() + " : " + decryptedMsg);
                // Append to the chat box if the current chatbox is the same as current source.
                if (!selectionFlag && inMsg.getSource().equalsIgnoreCase(controller.statusTable.getSelectionModel().getSelectedItem().getName())) {
                    controller.chatBox.appendText(inMsg.getSource() + " : " + EncryptingAlgo.decrypt(inMsg.getContent()) + "\n");
                }
            } else {
                controller.chatBox.appendText("Can't Find User\n");
            }
        }

        public void groupChat(Message inMsg, boolean selectionFlag) throws Exception {
            int listNum;
            // Receive Group Message
            // Find the group index from the list
            if ((listNum = Client.getListNum(inMsg.getDestination())) != -1) {
                String decryptedMsg = EncryptingAlgo.decrypt(inMsg.getContent());
                // Add the message to the group Name Object
                nameList.get(listNum).addMessage(inMsg.getSource() + " : " + decryptedMsg);
                database.writeChatHist(inMsg.getDestination(), listNum, inMsg.getSource() + " : " + decryptedMsg);
                // Append to the chat box if the current chatbox is the same group name
                if (!selectionFlag && inMsg.getDestination().equalsIgnoreCase(controller.statusTable.getSelectionModel().getSelectedItem().getName())) {
                    controller.chatBox.appendText(inMsg.getSource() + " : " + EncryptingAlgo.decrypt(inMsg.getContent()) + "\n");
                }
            } else {
                controller.chatBox.appendText("Can't Find Group");
            }
        }

        public void sendFile(Message inMsg, boolean selectionFlag) throws IOException, DataFormatException {
            int listNum;
//            System.out.println("Receive Sent File");
//            new Thread(new ReceiveFile(inMsg, ClientMain.echoSocket)).start();
//            synchronized (this) {
//                this.wait(1000);
//            }
            if (((listNum = Client.getListNum(inMsg.getSource())) != -1)) {
                // Add the message to the group Name Object
                nameList.get(listNum).addMessage("Receiving file from " + inMsg.getSource());
                database.writeChatHist(inMsg.getSource(), listNum, "Receiving file from " + inMsg.getSource());
                // Append to the chat box if the current chatbox is the same group name
                if (!selectionFlag && inMsg.getSource().equalsIgnoreCase(controller.statusTable.getSelectionModel().getSelectedItem().getName())) {
                    controller.chatBox.appendText("Receiving file from " + inMsg.getSource() + "\n");
                }
                ObjectInputStream ois = null;
                try {
                    //Create an input stream to receive file from server
                    ois = new ObjectInputStream(ClientMain.echoSocket.getInputStream());
                    
                    //Store the received file into content
                    byte[] content = (byte[]) ois.readObject();
                    
                    //Ask user whether want to save file and let user choose where to save
                    int dialogResult = JOptionPane.showConfirmDialog(null, "Would You Like to Save file from " + inMsg.getSource() + "?", "Warning " + inMsg.getDestination(), JOptionPane.YES_NO_OPTION);
                    if (dialogResult == JOptionPane.YES_OPTION) {
                        JFileChooser chooser = new JFileChooser();
                        chooser.setSelectedFile(new File(inMsg.getContent()));
                        chooser.showSaveDialog(null);
                        // Add the message to the group Name Object
                        nameList.get(listNum).addMessage("Received Size: " + content.length / 1024 + " Kb");
                        if (!selectionFlag && inMsg.getSource().equalsIgnoreCase(controller.statusTable.getSelectionModel().getSelectedItem().getName())) {
                            controller.chatBox.appendText("Received Size: " + content.length / 1024 + " Kb\n");
                        }
                        content = FileCompression.decompress(content);
                        // Add the message to the group Name Object
                        nameList.get(listNum).addMessage("Original Size: " + content.length / 1024 + " Kb");
                        if (!selectionFlag && inMsg.getSource().equalsIgnoreCase(controller.statusTable.getSelectionModel().getSelectedItem().getName())) {
                            controller.chatBox.appendText("Original Size: " + content.length / 1024 + " Kb\n");
                        }
                        Files.write(chooser.getSelectedFile().toPath(), content);
                    }
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                controller.chatBox.appendText("Can't Find User");
            }

        }

        public void sendGroupFile(Message inMsg, boolean selectionFlag) throws IOException, DataFormatException {
            int listNum;
//            System.out.println("Receive Sent File");
//            new Thread(new ReceiveFile(inMsg, ClientMain.echoSocket)).start();
//            synchronized (this) {
//                this.wait(1000);
//            }
            if (((listNum = Client.getListNum(inMsg.getDestination())) != -1)) {
                // Add the message to the group Name Object
                nameList.get(listNum).addMessage("Receiving file from " + inMsg.getSource());
                database.writeChatHist(inMsg.getDestination(), listNum, "Receiving file from " + inMsg.getSource());
                // Append to the chat box if the current chatbox is the same group name
                if (!selectionFlag && inMsg.getDestination().equalsIgnoreCase(controller.statusTable.getSelectionModel().getSelectedItem().getName())) {
                    controller.chatBox.appendText("Receiving file from " + inMsg.getSource() + "\n");
                }
                ObjectInputStream ois = null;
                try {
                    //Create an input stream to receive file from server
                    ois = new ObjectInputStream(ClientMain.echoSocket.getInputStream());
                    
                    //Store the received file into content
                    byte[] content = (byte[]) ois.readObject();
                    
                    //Ask user whether want to save file and let user choose where to save
                    int dialogResult = JOptionPane.showConfirmDialog(null, "Would You Like to Save file from\nGroup: " + inMsg.getDestination() + "\nSource: " + inMsg.getSource(), "File Sharing! User: " + ClientMain.controller.userNameLabel.getText(), JOptionPane.YES_NO_OPTION);
                    if (dialogResult == JOptionPane.YES_OPTION) {
                        JFileChooser chooser = new JFileChooser();
                        chooser.setSelectedFile(new File(inMsg.getContent()));
                        chooser.showSaveDialog(null);
                        // Add the message to the group Name Object
                        nameList.get(listNum).addMessage("Received Size: " + content.length / 1024 + " Kb");
                        if (!selectionFlag && inMsg.getDestination().equalsIgnoreCase(controller.statusTable.getSelectionModel().getSelectedItem().getName())) {
                            controller.chatBox.appendText("Received Size: " + content.length / 1024 + " Kb\n");
                        }
                        content = FileCompression.decompress(content);
                        // Add the message to the group Name Object
                        nameList.get(listNum).addMessage("Original Size: " + content.length / 1024 + " Kb");
                        if (!selectionFlag && inMsg.getDestination().equalsIgnoreCase(controller.statusTable.getSelectionModel().getSelectedItem().getName())) {
                            controller.chatBox.appendText("Original Size: " + content.length / 1024 + " Kb\n");
                        }
                        Files.write(chooser.getSelectedFile().toPath(), content);
                    }
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                controller.chatBox.appendText("Can't Find User");
            }
        }

    }

}
