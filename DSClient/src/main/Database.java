/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Angelyu
 */
public class Database {

    String folderPath;

    public Database() {
    }

    //Check Chat History Folder
    public void checkHistFolder(String userName) throws IOException {
        folderPath = userName;
        File dir = new File(userName);

        if (!dir.exists()) {
            dir.mkdir();
            System.out.println("folder created.");
        }

        for (int i = 0; i < Client.nameList.size(); i++) {
            checkReadChat(Client.nameList.get(i).getName(), i);
//            System.out.println(Client.nameList.get(i).getName());
        }
    }

    //Check chat history of every friend
    public void checkReadChat(String friendname, int index) throws IOException {
        String chatFilePath = folderPath + "\\" + friendname + ".txt";
        File file = new File(chatFilePath);
//        System.out.println(chatFilePath + "  " + friendname + "  " + file.exists() + "  " + file.toPath());
        if (!file.exists()) {
            file.createNewFile();
        } else {
            readChatHist(friendname, index);
        }
    }

    //Read chat history according to the friend name
    public void readChatHist(String friendname, int index) throws IOException {
        File f = new File(folderPath + "\\" + friendname + ".txt");
        BufferedReader reader = new BufferedReader(new FileReader(f));
        String content;

        while ((content = reader.readLine()) != null) {
            Client.nameList.get(index).addMessage(content);
        }
        reader.close();
        String sessionStr = "--------------New Session  " + (new Timestamp(new Date().getTime())).toString().substring(0, 16) + "--------------";
        Client.nameList.get(index).addMessage(sessionStr);
    }

    //Write chat history according to the friend name
    public void writeChatHist(String friendname, int index, String message) throws FileNotFoundException, IOException {

        File file = new File(folderPath + File.separator + friendname + ".txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
        writer.println(message);
        writer.close();

    }

    //Read Friend List from database
    public void readFriendList(String filePath) throws IOException {
        File f = new File(filePath);
        BufferedReader reader = new BufferedReader(new FileReader(f));
        PrintWriter writer = new PrintWriter(new FileWriter(f, true));
        String content;
        if (reader.readLine() == null) {

        } else {
            //Read friendlist from file
            while ((content = reader.readLine()) != null) {
                String[] friendListName = content.split("[;]");
                if (content.length() > 0) {
                    for (int i = 0; i < friendListName.length; i++) {
                        String names = friendListName[i];
                        System.out.println(names);
                    }
                }
            }
        }
    }

}
