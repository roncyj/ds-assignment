/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.File;
import static main.ClientMain.echoSocket;
import static main.ClientMain.out;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import static javafx.scene.input.KeyCode.ENTER;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import static main.Client.exec;
import static main.ClientMain.in;
import static main.ClientMain.port;

/**
 * FXML Controller class
 *
 * @author RonCYJ
 */
public class FXMLClientController implements Initializable {

    @FXML
    public TableView<Name> statusTable;
    @FXML
    private TableColumn<Name, String> chatName;
    @FXML
    private TableColumn<Name, String> chatType;
    @FXML
    private TableColumn<Name, String> chatStatus;
    @FXML
    private Button deleteFriendBtn;
    @FXML
    private Button newGroupBtn;
    @FXML
    public TextArea chatBox;
    @FXML
    public TextField messageBox;
    @FXML
    private AnchorPane loginPane;
    @FXML
    private AnchorPane messagingPane;
    @FXML
    private TextField userName;
    @FXML
    private PasswordField userPassword;
    @FXML
    private Button anonLoginBtn;
    @FXML
    private Button userLoginBtn;
    @FXML
    private Button signUpBtn;
    @FXML
    public Label userNameLabel;
    @FXML
    public Label currentChatName;
    private ObservableList<Name> chatData = FXCollections.observableArrayList();
    public int currentChat = -1;
    public boolean conn2ServerFlag = false;
    public boolean fileAcknowledge = false;
    public boolean anonymousFlag = false;
    public boolean connResetFlag = false;
    public boolean messageReadyFlag = true;
    Thread receiveTask;
    public Client.Receiver clientReceiver = new Client.Receiver();
    public Thread messageDelay;
    public MessageDelay messageDelayClass;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        chatName.setCellValueFactory(new PropertyValueFactory<Name, String>("name"));
        chatType.setCellValueFactory(new PropertyValueFactory<Name, String>("type"));
        chatStatus.setCellValueFactory(new PropertyValueFactory<Name, String>("status"));

        // Listen for table selection changes
        statusTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> {
                            // if (statusTable.getSelectionModel().getSelectedIndex() != currentChat) {
                            currentChat = statusTable.getSelectionModel().getSelectedIndex();
                            if (currentChat >= 0) {
                                chatBox.setText(Client.nameList.get(currentChat).getMessage());
                                currentChatName.setText(Client.nameList.get(currentChat).getName());
                            }
                            // }
                        });

        // Key Listener for message box (Area where user type message)
        messageBox.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
            switch (e.getCode()) {
                // Send Message when Enter is press
                case ENTER: {
                    try {
                        sendAction(null);
                    } catch (IOException ex) {
                        Logger.getLogger(FXMLClientController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(FXMLClientController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
            }
        });

        // Key Listener for User Login
        userPassword.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
            switch (e.getCode()) {
                case ENTER: {
                    try {
                        userLoginAction(null);
                        break;
                    } catch (IOException ex) {
                        Logger.getLogger(FXMLClientController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(FXMLClientController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(FXMLClientController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

    }

    @FXML
    private void sendAction(ActionEvent event) throws IOException, Exception {
        //Check whether message is ready to send again
        if (messageReadyFlag && !messageBox.getText().equalsIgnoreCase("")) {
            // Check whether the any name is selected
            if (statusTable.getSelectionModel().selectedItemProperty().getValue() == null) {
                alertDialog("No Selection", "No Item Selected", "Please select a Friend/Group in the table.");
                return;
            }
            // Get the selected item name
            String targetName = statusTable.getSelectionModel().getSelectedItem().getName();
            // Check whether the selected item is individual or group
            if (statusTable.getSelectionModel().getSelectedItem().getType().equalsIgnoreCase("Personal")) {
                //Sending Individual Message
                out.println(new Message("IndChat", targetName, userNameLabel.getText(), EncryptingAlgo.encrypt(messageBox.getText())).toString());
                chatBox.appendText("Me : " + messageBox.getText() + "\n");
                Client.nameList.get(Client.getListNum(targetName)).addMessage("Me : " + messageBox.getText());
                Client.database.writeChatHist(targetName, Client.getListNum(targetName), "Me : " + messageBox.getText());
                messageBox.setText("");
            } else {
                //Sending Group Message
                out.println(new Message("GroupChat", targetName, userNameLabel.getText(), EncryptingAlgo.encrypt(messageBox.getText())).toString());
                chatBox.appendText("Me : " + messageBox.getText() + "\n");
                Client.nameList.get(Client.getListNum(targetName)).addMessage("Me : " + messageBox.getText());
                Client.database.writeChatHist(targetName, Client.getListNum(targetName), "Me : " + messageBox.getText());
                messageBox.setText("");
            }

            //Change Message flag and notify MessageDelayClass to wait for 1second 
            messageReadyFlag = false;
            synchronized (messageDelayClass) {
                messageDelayClass.notify();
            }
        } else if (!messageBox.getText().equalsIgnoreCase("")) {
            System.out.println("PleaseWait");
            chatBox.appendText("***Slow Down*** Don't spam your friend\n");
        }
    }

    @FXML
    private void sendFileAction(ActionEvent event) throws IOException, InterruptedException {
        //Check whether message is ready to send again
        if (messageReadyFlag) {

            //Check whether name are selected or not
            if (statusTable.getSelectionModel().selectedItemProperty().getValue() == null) {
                alertDialog("No Selection", "No Item Selected", "Please select a Friend/Group in the table.");
                return;
            }

            // Choose a file to send
            FileChooser fileChosen = new FileChooser();
            fileChosen.setTitle("File to Send");
            File file = fileChosen.showOpenDialog(null);

            // Get the selected item name
            String targetName = statusTable.getSelectionModel().getSelectedItem().getName();

            //Check whether user is online or not
            if (!statusTable.getSelectionModel().getSelectedItem().getStatus().equalsIgnoreCase("Offline")) {
                //Display what file the user is sending 
                chatBox.appendText("Me : Send a file -> " + file.getName() + "\n");
                Client.nameList.get(Client.getListNum(targetName)).addMessage("Me : Send a file -> " + file.getName());
                Client.database.writeChatHist(targetName, Client.getListNum(targetName), "Me : Send a file -> " + file.getName());

                //Read the selected files by user
                byte[] content = Files.readAllBytes(file.toPath());
                chatBox.appendText("Original Size: " + content.length / 1024 + " Kb\n");
                Client.nameList.get(Client.getListNum(targetName)).addMessage("Original Size: " + content.length / 1024);
                Client.database.writeChatHist(targetName, Client.getListNum(targetName), "Original Size: " + content.length / 1024);

                //Compress the content of the file
                content = FileCompression.compress(content);
                chatBox.appendText("Compressed Size: " + content.length / 1024 + " Kb\n");
                Client.nameList.get(Client.getListNum(targetName)).addMessage("Compressed Size: " + content.length / 1024 + " Kb");
                Client.database.writeChatHist(targetName, Client.getListNum(targetName), "Compressed Size: " + content.length / 1024);

                //Check whether it's a group or individual and send
                if (statusTable.getSelectionModel().getSelectedItem().getType().equalsIgnoreCase("Personal")) {
                    out.println(new Message("SendFile", targetName, userNameLabel.getText(), file.getName()).toString());
                } else {
                    out.println(new Message("SendGroupFile", targetName, userNameLabel.getText(), file.getName()).toString());
                }

                // Wait Server to setup the ObjectInputStream
                synchronized (this) {
                    this.wait(500);
                }

                //Send file to server using ObjectOutputStream
                ObjectOutputStream oos = new ObjectOutputStream(echoSocket.getOutputStream());
                oos.writeObject(content);
            } else {
                chatBox.appendText("File not Send \nError: " + targetName + " is Not Online.");
            }

            //Change Message flag and notify MessageDelayClass to wait for 1second 
            messageReadyFlag = false;
            synchronized (messageDelayClass) {
                messageDelayClass.notify();
            }
        } else {
            System.out.println("PleaseWait");
            chatBox.appendText("***Slow Down*** Don't spam your friend\n");
        }
    }

    @FXML
    private void newGroupAction(ActionEvent event) throws IOException, Exception {
        String groupName = "";
        String memberList = "";

        //Create a Dialog to ask user enter a name for the Group 
        TextInputDialog dialog = new TextInputDialog("GroupName");
        dialog.setTitle("Text Input Dialog");
        dialog.setHeaderText("Look, a Text Input Dialog");
        dialog.setContentText("Please enter your name:");
        Optional<String> result = dialog.showAndWait();

        //Check whether group name is present or not
        if (result.isPresent()) {
            groupName = result.get();

            //Get the name list and let user choose which user to add into Group
            List<String> choices = new ArrayList<>();
            for (int i = 0; i < Client.nameList.size(); i++) {
                if (Client.nameList.get(i).getType().equalsIgnoreCase("Personal")) {
                    choices.add(Client.nameList.get(i).getName());
                }
            }
            //Remove the user own name on the choices list
            choices.remove(userNameLabel.getText());

            String tempStr = ""; // Temporary String to show user the previous added Username
            String memberStr = ""; // String to be send to server to create a group

            while (!choices.isEmpty()) {
                ChoiceDialog<String> choiceDialog = new ChoiceDialog<>(choices.get(0), choices);
                choiceDialog.setTitle("Add Member for Group");
                choiceDialog.setHeaderText("Select a name and press OK to add into group\nPress cancel to finish the process\n\nAdded: " + tempStr);
                choiceDialog.setContentText("Please choose");

                //Create dialog and let user choose
                Optional<String> choiceResult = choiceDialog.showAndWait();

                //If the user choose a username, it will add the chosen name into the String
                if (choiceResult.isPresent()) {
                    System.out.println("Your choice: " + choiceResult.get());
                    memberList += choiceResult.get() + ";";
                    memberStr += choiceResult.get() + "\n";
                    tempStr = choiceResult.get();
                    choices.remove(choiceResult.get());
                } else {
                    //Break if User choose cancel
                    break;
                }
            }

//            System.out.println(memberList);
            //Check whether member list is empty or not
            if (!memberList.equalsIgnoreCase("")) {
                //Include own username to the memberlist
                memberList = userNameLabel.getText() + ";" + memberList;

                //Send request to create a group
                out.println(new Message("NewGroup", groupName, userNameLabel.getText(), memberList).toString());

                //Receive respond from server
                Message inMsg = new Message(in.readLine());
                if (inMsg.getContent().equalsIgnoreCase("Created")) {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Group Created");
                    alert.setHeaderText(null);
                    alert.setContentText("Group is created!\n\nMember List: \n" + memberStr);
                    alert.showAndWait();

                    //Send a message to all the group Member to inform about the group
                    out.println(new Message("GroupChat", groupName, userNameLabel.getText(), EncryptingAlgo.encrypt("\n-----------------\nMember List:\n" + memberStr + "-----------------")));
                    chatBox.appendText("Me : " + "\n-----------------\nMember List:\n" + memberStr + "-----------------");
                    Client.nameList.get(Client.getListNum(groupName)).addMessage("Me : " + "\n-----------------\nMember List:\n" + memberStr + "-----------------");
                    Client.database.writeChatHist(groupName, Client.getListNum(groupName), "Me : " + "\n-----------------\nMember List:\n" + memberStr + "-----------------");
                } else {
                    alertDialog("Error!", "Unable to create Group", "Please choose another name");
                }
            } else {
                alertDialog("No Selection", "No User is Selected", "Please create the group again");
            }
            // The Java 8 way to get the response value (with lambda expression).
//            choiceResult.ifPresent(letter -> System.out.println("Your choice: " + letter));
        } else {
            System.out.println("No Group Name");
        }
    }

    // refresh status table
    public void refreshStatusTable(ArrayList<Name> nameList) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                int selectedIndex = statusTable.getSelectionModel()
                        .getSelectedIndex();
                statusTable.getSelectionModel().clearSelection();
                chatData.clear();
                chatData.addAll(nameList);
                statusTable.setItems(null);
                statusTable.layout();
                statusTable.setItems(chatData);
                statusTable.getSelectionModel().select(selectedIndex);
            }
        });
    }

    // when there are no selection, display error message
    public void alertDialog(String title, String header, String content) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();

        //Deprecated -  org.controlsfx.dialog.Dialogs;
        //Dialogs.create().title("No Selection").masthead("No Item Selected")
        //        .message("Please select a Friend/Group in the table.").showError();
    }

    //Connect to the server
    public void connect2Server(int portNum) {
        try {
            //Initialize Socket, input and output stream
            echoSocket = new Socket(InetAddress.getLocalHost().getHostName(), portNum);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
            System.out.println("End IO initialize");
            System.out.println("Connected to Server port: " + portNum + "\n");
            conn2ServerFlag = true;
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host taranis");
            System.exit(1);
        } catch (IOException e) {
            //If unable to reach the port, will increment the port and search for server
            System.err.println("Couln't get I/O for the connection to the name you used for your socket. " + portNum);
            portNum++;
            if (portNum < 8083) {
                connect2Server(portNum);
            } else {
                System.exit(1);
            }
        }
    }

    @FXML
    public void anonLoginAction(ActionEvent event) throws IOException, InterruptedException {
        if (!conn2ServerFlag) {
            connect2Server(port);
        }
        loginPane.setVisible(false);
        messagingPane.setVisible(true);

        //Request for Anonymous login from server
        out.println(new Message("Login", "", "Anon", "").toString());

        //Get server respond
        Message inMsg = new Message(in.readLine());

        //If server respond with "Status" message type, means it gain access
        if (inMsg.getType().equals("Status")) {
            anonymousFlag = true;
            authenticated(inMsg);
        } else {
            alertDialog("Access Denied", "Wrong User Name/Password", "Please enter the correct UserName/Password");
            conn2ServerFlag = false;
        }
    }

    @FXML
    public void userLoginAction(ActionEvent event) throws IOException, ClassNotFoundException, InterruptedException {
        if (!conn2ServerFlag) {
            connect2Server(port);
        }

        //Request for authentication with username and password
        Message msg = new Message("Login", "", userName.getText(), userPassword.getText());
        out.println(msg.toString());

        //Get server respond
        Message inMsg = new Message(in.readLine());

        //If server respond with "Status" message type, means it gain access
        if (inMsg.getType().equals("Status")) {
            authenticated(inMsg);
        } else if (inMsg.getType().equals("Denied") && inMsg.getContent().equals("LoggedIn")) {
            alertDialog("Access Denied", "User is Logged in", "If it's not you that Logged In\nPlease contact the server Support Team");
            conn2ServerFlag = false;
        } else {
            alertDialog("Access Denied", "Wrong User Name/Password", "Please enter the correct UserName/Password");
            conn2ServerFlag = false;
        }
    }

    public void authenticated(Message inMsg) throws IOException, InterruptedException {
        System.out.println("User Name: " + inMsg.getDestination());
        userNameLabel.setText(inMsg.getDestination());
        loginPane.setVisible(false);
        messagingPane.setVisible(true);
        exec.execute(new Client.UpdateStatus(inMsg.getContent().split("[;]")));
        receiveTask = new Thread(clientReceiver);
        receiveTask.start();
        messageDelayClass = new MessageDelay();
        messageDelay = new Thread(messageDelayClass);
        messageDelay.start();
        userPassword.setText("");
    }

    @FXML
    public void signUpAction(ActionEvent event) throws IOException, ClassNotFoundException, InterruptedException {
        if (!userName.getText().isEmpty() && !userPassword.getText().isEmpty()) {
            if (!conn2ServerFlag) {
                connect2Server(port);
            }
            
            //Request for Signup
            Message msg = new Message("SignUp", "", userName.getText(), userPassword.getText());
            out.println(msg.toString());
            
            //Get Server Respond
            Message inMsg = new Message(in.readLine());
            if (inMsg.getContent().equalsIgnoreCase("Created")) {
                userLoginAction(null);
            } else {
                alertDialog("Error!", "Unable to create new account", "Please choose different username");
            }
        } else {
            alertDialog("Error!", "Unable to create new account", "Please enter username and password!");
        }
    }

    @FXML
    public void signOutAction(ActionEvent event) throws IOException {
        //Request to logout
        out.println(new Message("Logout", "", "", "LogOut").toString());
        loginPane.setVisible(true);
        messagingPane.setVisible(false);
        conn2ServerFlag = false;
        
        //Stop all the running thread for the user accessed
        messageDelayClass.interrupt();
        synchronized (messageDelayClass) {
            messageDelayClass.notify();
        }
        receiveTask.stop();
    }

    //MessageDelay is to prevent user from Keep spamming  which might cause DDOS if doesn't implement this
    public class MessageDelay implements Runnable {

        boolean interrupt = false;

        @Override
        public void run() {
            try {
                while (!interrupt) {
                    synchronized (this) {
                        this.wait();
                        this.wait(1000);
                        messageReadyFlag = true;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(FXMLClientController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        public void interrupt() {
            interrupt = true;
        }
    }
}
