/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import static javafx.application.Application.launch;

/**
 *
 * @author RonCYJ
 */
public class ClientMain extends Application {

    public static Socket echoSocket = null;
    public static PrintWriter out = null;
    public static BufferedReader in = null;
    public static FXMLClientController controller;
    public final static int port = 8080;

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoad = new FXMLLoader(getClass().getResource("FXMLClient.fxml"));
        Parent root = fxmlLoad.load();
        Scene scene = new Scene(root);
        controller = fxmlLoad.<FXMLClientController>getController();
        stage.setScene(scene);
        stage.show();

        // Automatic disconnect from server when exit the application
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                try {
                    if (controller.conn2ServerFlag) {
                        controller.signOutAction(null);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClientMain.class.getName()).log(Level.SEVERE, null, ex);
                }
                Platform.exit();
                System.exit(0);

            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
