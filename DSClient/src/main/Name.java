/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author RonCYJ
 */
public class Name {

    private String name;
    private String type;
    private String status = "Offline";
    private String message = ""; // To store the message from the source

    public Name(String name, String type, String status) {
        this.name = name;
        this.type = type;
        this.status = status;
    }

    public Name(String userName) {
        this.name = userName;
    }

    /**
     * @return the userName
     */
    public String getName() {
        return name;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void addMessage(String str) {
        message = message + str + "\n";
    }

    public String toList() {
        return name + ";" + type;
    }
}
