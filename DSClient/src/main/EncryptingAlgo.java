package main;

import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.*;

public class EncryptingAlgo {

    private static final String algoType = "AES";
    //this is the shared key material used to generate secret key (not the real secret key)
    private static final byte[] keyValue = new byte[]{'B', '6', '9', 'A', '9', '8', 'D', 'D', '8', '2', '9', '1', '8', '%', '#', '&'};

    public static String encrypt(String Data) throws Exception {

        Key encKey = generateKey();
        Cipher code = Cipher.getInstance(algoType);
        code.init(Cipher.ENCRYPT_MODE, encKey);

        //changing plaintext into ciphertext using a secret key
        byte[] encryptionValue = code.doFinal(Data.getBytes());

        //change byte array of ciphertext into a String
        String encryptedValue = new BASE64Encoder().encode(encryptionValue);
        return encryptedValue;
    }

    public static String decrypt(String encryptedData) throws Exception {

        Key encKey = generateKey();
        Cipher code = Cipher.getInstance(algoType);
        code.init(Cipher.DECRYPT_MODE, encKey);

        //converting ciphertext in String back into a byte array
        byte[] decodingValue = new BASE64Decoder().decodeBuffer(encryptedData);

        //changing ciphertext back to plaintext
        byte[] decValue = code.doFinal(decodingValue);

        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    //method that generates a secret key from key material
    private static Key generateKey() throws Exception {

        Key encKey = new SecretKeySpec(keyValue, algoType);
        return encKey;

    }

}
